#include <stdio.h>

#define MAXBUF 5000
char buffer[MAXBUF];

main(int argc, char *argv[])
{
        int fp, nchar;

        fp = open(argv[1], 0);
        nchar = read(fp, buffer, MAXBUF); 
        close(fp);

        printf("%s", buffer);
}
