#define LETTER 'a'
#define DIGIT  '0'


getword (char *w, int lim)       /* get next word from input */
{
        int c, t;

        if (type(c = *w++ = getch()) != LETTER) {
                *w = '\0';
                return(c);
        }
        while (--lim > 0) {
                t = type (c = *w++ = getch());
                if (t != LETTER && t != DIGIT) {
                        ungetch(c);
                        break;
                }
        }
        *(w-1) = '\0';
        return (LETTER);
}

type(int c)         /* return type of ASCII character */
{
        if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
                return (LETTER);
        else if (c >= '0' && c <= '9')
                return (DIGIT);
        else
                return (c);
}



