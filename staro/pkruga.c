#include <stdio.h>
#include <math.h>
#define PI 3.1415926
#define KVADRAT(x) ((x) * (x))

double povrsina;
double povrsina_kruga (double);

main (int argc, char **argv)
{
        double radius;
        if (argc < 2) exit (0);

        radius = atof (argv[1]);
        povrsina = povrsina_kruga (radius);
        printf ("Povrsina kruga poluprecnika %f je %f\n",
                                                radius, povrsina);
}

double povrsina_kruga (double r)
{
        return (PI * KVADRAT(r));
}


