#include <stdio.h>
typedef struct k {int x, y;} CMP;    
CMP mnozi(CMP p, CMP q);
stam(CMP p, char x);
main()
{
     CMP a = {3, 4}; CMP b = {-8, 1};
     stam(a,'*'); stam(b,'='); stam(mnozi(a, b),'\n');
}
CMP mnozi(CMP p, CMP q)
{
    CMP r;
    r.x = p.x*q.x -p.y*q.y;
    r.y = p.x*q.y + p.y*q.x;
    return(r);
}
stam(CMP p, char x)
{
    printf("(%d,%d)%c", p.x, p.y, x);
}
