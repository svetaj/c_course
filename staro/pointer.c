#include <stdio.h>
main()
{
        char *a[] = {"abcdef", "pqr", "xyz12", "1234"};

        printf("abcdef pqr xyz12 1234\n");

        printf(" a[2]        = %s \n", a[2]        );
        printf(" a[2][1]     = %c \n", a[2][1]     );
        printf(" *(a+1)      = %s \n", *(a+1)      );
        printf(" (*(a+1))[2] = %c \n", (*(a+1))[2] );
        printf(" *(*(a+1)+2) = %c \n", *(*(a+1)+2) );
}
