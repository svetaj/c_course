#include <stdio.h>

main()    /* broji cifre, blankove i drugo */
{
        int c, i, brbelo, brdrugo;
        int brcifara[10];

        brbelo = brdrugo = 0;
        for (i = 0; i < 10; ++i)
                brcifara[i] = 0;

        while ((c = getchar()) != EOF)
                if (c >= '0' && c <= '9')
                        ++brcifara[c - '0'];
                else if (c == ' ' || c == '\n' || c == '\t')
                        ++brbelo;
                else
                        ++brdrugo;
        printf ("brojevi=");
        for (i = 0; i < 10; ++i)
                printf (" %d", brcifara[i]);
        printf ("\nblankovi = %d, drugo = %d\n", brbelo, brdrugo);
}

