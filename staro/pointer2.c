#include <stdio.h>

main()
{
        int *niz_pointera[4];
        int niz1[8] = {99, -1, 35, 45, 28, 0, 3, 8};
        int niz2[5] = {62, -100, 33, 2, 1};
        int vrednost = 1000;
        int i;

        niz_pointera[0] = niz1;
        niz_pointera[1] = niz2;
        niz_pointera[2] = &vrednost;
        niz_pointera[3] = niz1 + 3;

        /* prikazuje samo prvu vrednost za sve pointere */
        for(i = 0; i < 4; i++)
                printf("[ %d ][...]\n", *(niz_pointera[i]) );

        /* prikazuje sve vrednosti za prvi pointer, na tri nacina */

        for(i = 0; i < 8; i++)
                printf("[ %d ]", niz_pointera[0][i] );
        printf("\n");

        for(i = 0; i < 8; i++)
                printf("[ %d ]", *(niz_pointera[0] + i) );
        printf("\n");

        for(i = 0; i < 8; i++)
                printf("[ %d ]", *(*niz_pointera + i) );
        printf("\n");

}

