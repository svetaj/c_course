#include <stdio.h>
#include <string.h>

struct datum {
        int dan;
        int mesec;
        int godina;
};

struct radnik {
        char ime[20];
        char prezime[20];
        struct datum rodjen;
        struct datum radi;
        double plata;
        char adresa[40];
};

main()
{
        struct radnik pera;
        struct radnik mika = { "Miki",
                               "Maus",
                               {1,1,1904},
                               {10,10,1950},
                               15.13,
                               "Ulica brestova 1313"
                             };

        strcpy (pera.ime, "Petar");
        strcpy (pera.prezime, "Petrovic");
        pera.rodjen.dan =13;
        pera.rodjen.mesec = 6;
        pera.rodjen.godina = 1950;
        pera.radi.dan = 2;
        pera.radi.mesec = 9;
        pera.radi.godina = 1979;
        pera.plata = 2550.30;
        strcpy(pera.adresa, "Glavna 32, Zemun");

        printf("IME:     %s\n", pera.ime);
        printf("PREZIME: %s\n", pera.prezime);
        printf("RODJEN:  %d/%d/%d\n", pera.rodjen.dan,
                                      pera.rodjen.mesec,
                                      pera.rodjen.godina);
        printf("RADI OD: %d/%d/%d\n", pera.radi.dan,
                                      pera.radi.mesec,
                                      pera.radi.godina);
        printf("PLATA:   %.1f\n", pera.plata);
        printf("ADRESA:  %s\n", pera.adresa);
        printf("\n\n");

        printf("IME:     %s\n", mika.ime);
        printf("PREZIME: %s\n", mika.prezime);
        printf("RODJEN:  %d/%d/%d\n", mika.rodjen.dan,
                                      mika.rodjen.mesec,
                                      mika.rodjen.godina);
        printf("RADI OD: %d/%d/%d\n", mika.radi.dan,
                                      mika.radi.mesec,
                                      mika.radi.godina);
        printf("PLATA:   %.1f\n", mika.plata);
        printf("ADRESA:  %s\n", mika.adresa);

}


