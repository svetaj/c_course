

Classification routines           Conversion routines
Directory control routines        Diagnostic routines
Graphics routines                 Inline routines
Input/output routines             Interface routines
Math routines                     Memory routines
Miscellaneous routines            Process control routines
Standard routines                 String and memory routines
Text window display routines      Time and date routines
Variable argument list routines

 Standard routines

These are standard routines. They are declared in stdlib.h.

  abort     abs       atexit    atof      atoi      atol      bsearch
  calloc    ecvt      _exit     exit      fcvt      free      gcvt
  getenv    itoa      labs      lfind     lsearch   ltoa      malloc
  putenv    qsort     rand      realloc   srand     strtod    strtol
  swab      system

