all: tree.exe

tree.obj: tree.c
        bcc -c tree.c

getword.obj: getword.c
        bcc -c getword.c

iolib.obj: iolib.c
        bcc -c iolib.c

alloc.obj: alloc.c
        bcc -c alloc.c

tree.exe: tree.obj getword.obj iolib.obj alloc.obj
        bcc -etree tree.obj getword.obj iolib.obj alloc.obj

