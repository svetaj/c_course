#include <math.h>
#include "cmath.h"


/*===================================================================*/
/* funkcija: moduo (abs. vrednost) kompleksnog broja                 */
/*===================================================================*/

double zabs (struct kompleks a)
{
    return(sqrt(a.x*a.x + a.y*a.y));
}


/*===================================================================*/
/* funkcija: mnozenje dva kompleksna broja                           */
/*===================================================================*/

void cmult (struct kompleks a, struct kompleks b, struct kompleks *c)
{
    c->x = a.x * b.x - a.y * b.y;
    c->y = a.x * b.y + a.y * b.x;
}


/*===================================================================*/
/* funkcija: oduzimanje dva kompleksna broja                         */
/*===================================================================*/

void csub (struct kompleks a, struct kompleks b, struct kompleks *c)
{
    c->x = a.x - b.x;
    c->y = a.y - b.y;
}


/*===================================================================*/
/* funkcija: sabiranje dva kompleksna broja                          */
/*===================================================================*/

void cadd (struct kompleks a, struct kompleks b, struct kompleks *c)
{
    c->x = a.x + b.x;
    c->y = a.y + b.y;
}

/*===================================================================*/
/* funkcija: mnozenje kompleksnog broja skalarom                     */
/*===================================================================*/

void smult (struct kompleks a, double b, struct kompleks *c)
{
    c->x = a.x * b;
    c->y = a.y * b;
}


/*===================================================================*/
/* funkcija:  deljenje dva kompleksna broja                          */
/*===================================================================*/

void cdiv (struct kompleks a, struct kompleks b, struct kompleks *c)
{
    double bb;
    bb = b.x*b.x + b.y*b.y;
    c->x = (a.x * b.x + a.y * b.y)/bb;
    c->y = (a.y * b.x - a.x * b.y)/bb;
}
