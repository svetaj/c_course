#include <math.h>
#include <stdio.h>



/*=========================================================================*/
/*  podprogram: trazi nulu funkcije y = f(x) newton - raphson-ovim         */
/*              metodom                                                    */
/*                                                                         */
/*  ulazne promenljive:                                                    */
/*                                                                         */
/*    y    = eksterna funkcija za koju se trazi nula                       */
/*    x1   = donja granica opsega u kojem ocekujemo nulu funkcije          */
/*    x2   = gornja granica opsega u kojem ocekujemo nulu funkcije         */
/*    n    = dozvoljeni broj iteracija                                     */
/*                                                                         */
/*  izlazne promenljive:                                                   */
/*                                                                         */
/*    xz     = nula funkcije u opsegu [x1,x2]                              */
/*    ind    = indikator resenja, ind=0 - nema resenja,                    */
/*                                ind=1 - pronadjeno je resenje            */
/*=========================================================================*/


double nrzero (double (*y)(double x), double xr1, double xr2, int n)
{
      double x1, x2, y1, y2, accy, x3, y3, x_zero;
      int icoun, icmax;

/*   inicijalizacija promenljivih  */

      x1 = xr1;
      x2 = xr2;
      y1 = (*y)(x1);
      y2 = (*y)(x2);
      icoun = 0;
      icmax = n;
      accy = 1.0e-20;

/* provera da li su zadovoljeni uslovi za vaznost newton-raphson-ove */
/* teoreme                                                           */

      if(y1*y2 > 0) {
	  x_zero = 0.;			     /* Ne vazi teorema */
	  goto end;
      }

/* provera da li je nula na granicama opsega */

      if(fabs(y1) < accy) {
	  x_zero = x1;
	  goto end;
      }

      if(fabs(y2) < accy) {
	  x_zero = x2;
	  goto end;
      }

/* pocetak iterativnog metoda: opseg se deli na dva dela */

lab:
      x3 = (x1+x2)/2.;
      icoun++;
      y3 = (*y)(x3);

/* ako je dostignut maksimalni broj iteracija usvaja se priblizno resenje */

      if(icoun > icmax) {
	  x_zero = x3;
	  goto end;
      }

/*  provera da li nova tacka predstavlja nulu  */

      if (fabs(y3) < accy) {
	  x_zero = x3;
	  goto end;
      }

/* nastavak iterativnog metoda: priprema za ponovno deljenje */
/* segmenta no. 1 ili no. 2 na dva dela                      */

      else if(y1*y3 < 0.) {
	  x2 = x3;
	  y2 = y3;
	  goto lab;
      }
      else if(y2*y3 < 0.) {
	  x1 = x3;
	  y1 = y3;
	  goto lab;
      }
      else {
	  pmsg(" nrzero: resenje nije pronadjeno !");
	  goto end;
      }
end:
      return(x_zero);
}

