#include <stdio.h>

#define MAXVAL  100     /* maximum depth of val stack */

int sp = 0;             /* stack pointer */
double val[MAXVAL];     /* value stack */

double push(f)          /* push f onto value stack */
double f;
{
        if (sp < MAXVAL) {
                val[sp++] = f;
                stam(0);
                return (f);
        }
        else {
                printf ("error: stack full\n");
                clear();
                return (0);
        }
}

double pop()            /* pop top value from stack */
{
        double f;
        if (sp > 0) {
                f = val[--sp];
                stam(1);
                return (f);
        }
        else {
                printf("error: stack empty\n");
                clear();
                return(0);
        }
}

clear()                 /* clear stack */
{
        sp = 0;
}


stam(int j)
{
        int i;

        printf ("%s: ", (j == 0) ? "PUSH" : "POP ");
        for (i=0; i<10; i++) {
             printf ("|%c", (sp == i) ? '*' : ' ');
             printf ("%.1f", val[i]);
        }
        printf ("|\n");
}

