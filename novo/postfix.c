#include <stdio.h>

char ch;

trazi()
{
     do {
          ch = getchar();
     } while (ch == ' ' || ch == '\n');
}

cinilac()
{
     if (ch == '(') {
         trazi();
         izraz();
     } else
         putchar(ch);
     trazi();
}

zavrsi()
{
     cinilac();
     while (ch == '*') {
         trazi();
         cinilac();
         putchar('*');
         flushall();
    }
}

izraz()
{
    char op;

     zavrsi();
     while (ch == '+' || ch == '-') {
          op = ch;
          trazi();
          zavrsi();
          putchar(op);
     }
}

main()   /* konverzija u postfix format , Jensen & Wirth, p. 75 */
{
     trazi();
     do {
          putchar(' ');
          izraz();
          putchar('\n');
     } while(ch != '.');
}
     
