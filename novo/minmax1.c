#include <stdio.h>

#define N 20

/* nalazi maksimalni i minimalni element niza */

minmax(int a[], int dim, int *max, int *min)
{
    int i;                           /* prosledjujemo dim !!! */
                                     /* max i min su pointeri */
    *max = *min = a[0];
    for ( i = 1; i < dim; i++) {
        if (a[i] > *max)
             *max = a[i];
        if (a[i] < *min)
             *min = a[i];
    }
}

main()
{
    int niz[N] = { 35, 68, 94, 7, 88, -5, -3, 12, 35, 9,
                   -6, 3, 0, -2, 74, 88, 52, 43, 5, 4 };
    int x1, x2;

    minmax(niz, N, &x1, &x2);
    printf("max=%d min=%d\n", x1, x2);
}
