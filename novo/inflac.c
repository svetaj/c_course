#include <stdio.h>

main()
{
     int i, n = 10;
     float w1, w2, w3;
     w1 = w2 = w3 = 1.0;
     i = 0;
     while(i < n) {
          w1 *= 1.07;
          w2 *= 1.08;
          w3 *= 1.10;
          printf("%2d %12.12e %12.12e %12.12e\n", i, w1, w2, w3);
          i++;
     }
}
