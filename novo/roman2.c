#include <stdio.h>

char *roman(int);

main()
{
     int x;

     for (x = 1; x < 5000; x *= 2) 
          printf("%5d %s\n", x, roman(x));
}

char *roman(int x)
{
     static struct roman {
        char r;
        int  a;
     } cifra[7] = { {'m', 1000}, {'d', 500}, {'c', 100}, {'l', 50},
                    {'x', 10}, {'v', 5}, {'i', 1} };
     static char temp[30];
     int i, j = 0;

     for (i = 0; i < 7; i++) 
          while (x >= cifra[i].a) {
              temp[j++] = cifra[i].r;
              x -= cifra[i].a;
          }
     temp[j] = '\0';
     return(temp);
}
