#include <stdio.h>

main()
{
        int x, y;

        for (y = 1; y < 5000; y *= 2)
        {
             x = y;
             printf("%5d ", x);
             while (x >= 1000) {
                 printf("m");
                 x -= 1000;
             }            
             if (x >= 500) {
                 printf("d");
                 x -= 500;
             }            
             while (x >= 100) {
                 printf("c");
                 x -= 100;
             }            
             if (x >= 50) {
                 printf("l");
                 x -= 50;
             }            
             while (x >= 10) {
                 printf("x");
                 x -= 10;
             }            
             if (x >= 5) {
                 printf("v");
                 x -= 5;
             }            
             while (x >= 1) {
                 printf("i");
                 x -= 1;
             }            
             printf("\n");
        }
}
