#include <stdio.h>
#include <math.h>

/* crta grafik funkcije f(x) = exp(-x) * sin(2*pi*x)   */

main()
{
     double d = 0.0625;    /* 1/16, 16 linija za interval [x, x+1] */
     int s = 32;           /* 32 karaktera sirina za interval [y, y+1] */
     int h1 = 34;          /* karakter pozicija x-ose */
     int h2 = 68;          /* sirina linije */
     double c = 6.28318;   /* 2*pi */
     int lim = 32;

     double x, y;
     int i, j, k, n;
     char a[69];

     for (j = 0; j < h2; j++)
         a[j] = ' ';
     for (i = 0; i <= lim; i++) {
         x = d*i;
         y = exp(-x) * sin(c * x);
         a[h1-1] = ':';
         n = y * s + h1;
         a[n-1] = '*';
         (n < h1) ? (k = h1) : (k = n);
         for (j = 0; j < k; j++)
              putchar(a[j]);
         putchar('\n');
         a[n-1] = ' ';
     }
}
