#include <stdio.h>

main()
{
     struct roman {
        char r;
        int  a;
     } cifra[7] = { {'m', 1000}, {'d', 500}, {'c', 100}, {'l', 50},
                    {'x', 10}, {'v', 5}, {'i', 1} };
     int x, y, i;

     for (y = 1; y < 5000; y *= 2) {
          x = y;
          printf("%5d ", x);
          for (i = 0; i < 7; i++) 
              while (x >= cifra[i].a) {
                  printf("%c", cifra[i].r);
                  x -= cifra[i].a;
              }            
          printf("\n");
     }
}
