#include <stdio.h>
#include <math.h>

/* Racuna kosinus pomocu razvoja u red  */
/* cos(x) = 1 - x**2/2! + x**4/4! - ... */

#define EPS 1e-9
#define KVAD(a) ((a) * (a))

double moj_cos(double x)
{
    double sx, s, t;
    int k;

    t = s = 1.;
    k = 0;
    sx = KVAD(x);
    while( fabs(t) > EPS*fabs(s) ) {  /* sve dok ima znacajnih */
          k += 2;                     /* clanova niza          */
          t = -t*sx/(k*(k-1));
          s += t;
    }
    return s;
}
    
main()  /* poredi rezultat sa pozivom funkcije cos() */
{
    double xx;

    xx = 1.534622222233e-01;
    printf("%12.12e %12.12e %12.12e\n", xx, moj_cos(xx), cos(xx));
    xx = 3.333333333333e-01;
    printf("%12.12e %12.12e %12.12e\n", xx, moj_cos(xx), cos(xx));
    xx = 5.000000000000e-01;
    printf("%12.12e %12.12e %12.12e\n", xx, moj_cos(xx), cos(xx));
    xx = 1.000000000000e-01;
    printf("%12.12e %12.12e %12.12e\n", xx, moj_cos(xx), cos(xx));
    xx = 3.141592653590e-01;
    printf("%12.12e %12.12e %12.12e\n", xx, moj_cos(xx), cos(xx));
}

