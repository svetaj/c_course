#include <stdio.h>

#define N 20

main()
{
    int a[N] = { 35, 68, 94, 7, 88, -5, -3, 12, 35, 9,
                 -6, 3, 0, -2, 74, 88, 52, 43, 5, 4 };
    int i, min, max;

    max = min = a[0];
    for ( i = 1; i < N; i++) {
        if (a[i] > max)
             max = a[i];
        if (a[i] < min)
             min = a[i];
    }
    printf("max=%d min=%d\n", max, min);
}
