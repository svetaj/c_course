/*=================================================================*/
/*  racuna rastojanje izmedju dve tacke                            */
/*                                                                 */
/*   ulazne promenljive:                                           */
/*                                                                 */
/*     xl,yl   =  koordinate prve tacke                            */
/*     xk,yk   =  koordinate druge tacke                           */
/*                                                                 */
/*   izlazne promenljive:                                          */
/*                                                                 */
/*     dst     =  rastojanje izmedju dve tacke                     */
/*=================================================================*/
#include <math.h>
float dist(float xl, float yl, float xk, float yk)
{
      float dst, xlk, ylk;
      xlk = xl - xk;
      ylk = yl - yk;
      dst = xlk * xlk + ylk * ylk;  
      return(sqrt(dst));  
}
