#include <stdio.h>
#include <alloc.h>

typedef struct node {
     char info;
     struct node *llink;
     struct node *rlink;
} PTR;

preorder(PTR *p)
{
     if (p != NULL) {
         putchar(p->info);
         preorder(p->llink);
         preorder(p->rlink);
     } 
}

inorder(PTR *p)
{
     if (p != NULL) {
         inorder(p->llink);
         putchar(p->info);
         inorder(p->rlink);
     } 
}

postorder(PTR *p)
{
     if (p != NULL) {
         postorder(p->llink);
         postorder(p->rlink);
         putchar(p->info);
     } 
}

PTR *enter(PTR *p)
{
     char ch;
     ch = getchar();
     putchar(ch);
     if (ch != '.') {
         if (p == NULL)
             p = (PTR *) malloc(sizeof(PTR));
         p->info = ch;
         p->llink = NULL;
         p->rlink = NULL;
         p->llink = enter(p->llink);
         p->rlink = enter(p->rlink);
     } else
         p = NULL;
     return(p);
}

PTR *root;

main()
{
     putchar(' ');   root = enter(root); putchar('\n');
     putchar(' ');   preorder(root);     putchar('\n');
     putchar(' ');   inorder(root);      putchar('\n');
     putchar(' ');   postorder(root);    putchar('\n');
}
