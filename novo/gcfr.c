#include <stdio.h>
#include <math.h>

/* Funkcija NZS - najveci zajednicki sadrzalac */
/* Rekurzivna verzija Euklidovog algoritma     */

long nzs (long prvi, long drugi)
{
     int temp;
	
     prvi = labs (prvi);
     drugi = labs (drugi);

     if (drugi == 0)
          return prvi;
     else
          return nzs(drugi, prvi % drugi);
}

main()
{
        printf("%d %d %d\n",  18,   27, nzs( 18,   27));
        printf("%d %d %d\n", 312, 2142, nzs(312, 2142));
        printf("%d %d %d\n",  61,   53, nzs( 61,   53));
        printf("%d %d %d\n",  98,  868, nzs( 98,  868));
        printf("%d %d %d\n",  13,   27, nzs( 13,   27));
}




