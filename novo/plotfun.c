#include <stdio.h>
#include <math.h>

/* crta grafik funkcije f(x) = exp(-x) * sin(2*pi*x)   */

main()
{
     double d = 0.0625;    /* 1/16, 16 linija za interval [x, x+1] */
     int s = 32;           /* 32 karaktera sirina za interval [y, y+1] */
     int h = 34;           /* karakter pozicija x-ose */
     double c = 6.28318;   /* 2*pi */
     int lim = 32;

     double x, y;
     int i, n;

     for (i = 0; i <= lim; i++) {
         x = d*i;
         y = exp(-x) * sin(c * x);
         n = y * s + h;
         while (n > 0) {
              putchar(' ');
              n--;
         }
         putchar('*');
         putchar('\n');
     }
}
