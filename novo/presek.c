#include <stdio.h>

void int2l (float a1, float b1, float c1, float a2, float b2, float c2,
            float *x, float *y, int *ind);

void main()   
{
      float x, y;
      int ind;

      int2l(0., 1., -2., 1., 0., -3., &x, &y, &ind);
      printf("ind=%d x=%5.2f y=%5.2f", ind, x, y);
}
