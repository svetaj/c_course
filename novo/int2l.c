/*=================================================================*/
/*  nalazi presek dve prave                                        */
/*                                                                 */
/*   ulazne promenljive:                                           */
/*                                                                 */
/*      a1,b1,c1  = koeficijenti prve prave a1x+b1y+c1=0           */
/*      a2,b2,c2  = koeficijenti druge prave a2x+b2y+c2=0          */
/*                                                                 */
/*   izlazne promenljive:                                          */
/*                                                                 */
/*      x,y       = presecna tacka                                 */
/*      ind       = brojac preseka                                 */
/*                  0 - prave su pralelne                          */
/*                  1 - prave se seku                              */
/*=================================================================*/

#include <math.h>
#define accy 1.0e-6

void int2l (float a1, float b1, float c1, float a2, float b2, float c2,
            float *x, float *y, int *ind)
{
      float det, dinv;

      det = a1 * b2 - a2 * b1;  
      if(fabs(det) < accy)            /* prave su paralelne */
           *ind = 0;
      else {                         /* presek dve linije  */
           dinv = 1.0 / det; 
           *x = (b1 * c2 - b2 * c1) * dinv;
           *y = (a2 * c1 - a1 * c2) * dinv; 
           *ind = 1;
      }
}
