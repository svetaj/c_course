#include <stdio.h>
#include <math.h>

/* Funkcija NZS - najveci zajednicki sadrzalac */
/* Iterativna verzija Euklidovog algoritma     */

long nzs (long prvi, long drugi)
{
     int temp;
	
     prvi = labs (prvi);
     drugi = labs (drugi);
	
     while (drugi > 0) {
          temp = (int) (prvi % drugi);
          prvi = drugi;
          drugi = (long) temp;
     }
     return prvi;
}

main()
{
        printf("%d %d %d\n",  18,   27, nzs( 18,   27));
        printf("%d %d %d\n", 312, 2142, nzs(312, 2142));
        printf("%d %d %d\n",  61,   53, nzs( 61,   53));
        printf("%d %d %d\n",  98,  868, nzs( 98,  868));
        printf("%d %d %d\n",  13,   27, nzs( 13,   27));
}




