#include <stdio.h>
#include <math.h>

/* crta grafik funkcije y = f(x) */

plotfun(double (*f)(), double d, int s, int h)
{
     int h1 = h;            /* karakter pozicija x-ose */
     int h2 = 2*h;          /* sirina linije */
     int lim = s;

     double x, y;
     int i, j, k, n;
     char a[69];

     for (j = 0; j < h2; j++)
         a[j] = ' ';
     for (i = 0; i <= lim; i++) {
         x = d*i;
         y = (*f)(x);
         a[h1-1] = ':';
         n = y * s + h1;
         a[n-1] = '*';
         (n < h1) ? (k = h1) : (k = n);
         for (j = 0; j < k; j++)
              putchar(a[j]);
         putchar('\n');
         a[n-1] = ' ';
     }
}

double expsin(double x)
{
     static double c = 6.28318;        /* 2*pi */
     return exp(-x) * sin(c * x);
}

/* crta grafik funkcije f(x) = exp(-x) * sin(2*pi*x)   */

main()
{
     double d = 0.0625;    /* 1/16, 16 linija za interval [x, x+1] */
     int s = 32;           /* 32 karaktera sirina za interval [y, y+1] */
     int h = 34;           /* karakter pozicija x-ose */

     plotfun(expsin, d, s, h);
}

