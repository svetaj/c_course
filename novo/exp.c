#include <stdio.h>

/* racuna x**y, gde je y pozitivan ceo broj */

#define KVAD(a) ((a) * (a))

double moj_exp(double x, int y)
{
      double z = 1.;

      while (y > 0) {
           while (!(y % 2)) {
               y /= 2;
               x = KVAD(x);
           }
           y--;
           z*= x;
      }
      return z;
}

main()
{
      double xx = 2.;
      int yy = 7;
      printf("%g %d %g\n", xx, yy, moj_exp(xx, yy));
}


