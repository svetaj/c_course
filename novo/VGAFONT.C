#include <dos.h>
#include <stdio.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <sys\types.h>
#include <sys\stat.h>

unsigned char buffer[5000];

void main(int argc, char *argv[])
{
    unsigned char far *p;
    int fp;
    int segaddr, offaddr;

    printf("VGAFONT V1.0 (C) Sveta 1994\n");    
    
    if (argc != 2)
	exit(0);

    fp = open (argv[1], O_RDONLY);
    if (fp == -1)
	exit(0);
    read (fp, (void *)buffer, (unsigned int) sizeof(buffer));
    close (fp);

    p = (unsigned char far *)(buffer+4);

    segaddr = FP_SEG(p);
    offaddr = FP_OFF(p);
    _asm {
	 MOV AH, 11h
	 MOV AL, 0h
	 MOV CH, 01h
	 MOV CL, 0h
	 MOV DX, 0h
	 MOV BL, 0h
	 MOV BH, 10h
	 MOV ES, segaddr
	 MOV BP, offaddr
	 INT 10h
    }
    exit(0); 
}
