#include <stdio.h>

int strlen(char s[]);

main()   /* proverava upotrebu funkcije itoa() */
{
        char a[40];
        int i;

        i = -3452;
        itoa(i, a);

        printf("Broj predstavljen kao niz karaktera %s\n", a);
}

int itoa(int n, char s[])  /* convert n to characters in s */
{
        int i, sign;

        if ((sign = n) < 0)     /* record sign */
                n = -n;         /* make n positive */
        i = 0;
        do {            /* generate digits in reverse order */
                s[i++] = n % 10 + '0';  /* get next digit */
        } while ((n /= 10 ) > 0);       /* delete it */
        if (sign < 0)
                s[i++] = '-';
        s[i] = '\0';
        reverse(s);
}

reverse(char s[])       /* reverse string s in place */
{
        int c, i, j;

        for ( i= 0, j = strlen(s) - 1; i < j; i++, j--) {
                c = s[i];
                s[i] = s[j];
                s[j] = c;
        }
}

int strlen(char s[]) /* return length of s */
{
        int i;

        i = 0;
        while (s[i] != '\0')
                ++i;
        return(i);
}




