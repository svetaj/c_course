#include <stdio.h>

main()
{
        int a, b;

        a = 1;
        b = 99;

        printf("pre   swap: a=%d b=%d\n", a, b);
        swap(&a, &b);
        printf("posle swap: a=%d b=%d\n", a, b);
}

swap(int *px, int *py)      /* WRONG */
{
        int temp;

        temp = *px;
        *px = *py;
        *py = temp;
}
