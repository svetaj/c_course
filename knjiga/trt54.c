#include <stdio.h>

/* pronalazi na kojoj poziciji se u vektoru nalazi zadata vrednost */
/* vektor mora da bude sortiran */

main()
{
        int i;
        int x[10] = {1, 11, 13, 16, 22, 28, 37, 51, 91, 444};

        printf ("Sortiran vektor \n");
        for (i=0; i<10; i++)
                printf ("x[%d]=%d ", i, x[i]);
        printf ("\n");

        i = 37;
        printf ("Vrednost %d pronadjena na mestu %d\n",
                                        i, binary(i, x, 10));
}


binary(x, v, n)         /* find x in v[0] ... v[n-1]   */
int x, v[], n;
{
        int low, high, mid;

        low = 0;
        high = n - 1;
        while (low <= high) {
                mid = (low+high) / 2.;
                if (x < v[mid])
                        high = mid -1;
                else if (x > v[mid])
                        low = mid + 1;
                else    /* found match */
                        return (mid);
        }
        return (-1);
}


