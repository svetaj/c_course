#include <stdio.h>

int strcmp(char *s, char *t);

main()
{
        char a[] = "Petrovic Petar";
        char b[] = "Petrovic Dragan";
        char c[] = "Jovanovic Sveta";

        char x[] = "Petrovic Dragan";

        if (strcmp(x, a) < 0)
                printf("%s < %s\n", x, a);
        else if (strcmp(x, a) == 0)
                printf("%s == %s\n", x, a);
        else if (strcmp(x, a) > 0)
                printf("%s > %s\n", x, a);

        if (strcmp(x, b) < 0)
                printf("%s < %s\n", x, b);
        else if (strcmp(x, b) == 0)
                printf("%s == %s\n", x, b);
        else if (strcmp(x, b) > 0)
                printf("%s > %s\n", x, b);

        if (strcmp(x, c) < 0)
                printf("%s < %s\n", x, c);
        else if (strcmp(x, c) == 0)
                printf("%s == %s\n", x, c);
        else if (strcmp(x, c) > 0)
                printf("%s > %s\n", x, c);

}

int strcmp(char *s, char *t)   /* return <0 if s<t, 0 if s==t, >0 if s>t */
{                              /* pointer version */
        int i;

        for ( ; *s == *t; s++, t++)
                if (*s == '\0')
                        return(0);
        return(*s - *t);
}

