#include <stdio.h>

int getint(int *);

# define SIZE 100

/* testira upotrebu funkcije getint() na primeru popunjavanja */
/* niza tipa int zadavanjem elemenata sa tastature            */

main()
{
        int i, n, v, array[SIZE];

        for (n = 0; n < SIZE && getint(&v) != EOF; n++)
                array[n] = v;

        for (i = 0; i < n; i++)
                printf("array[%d]=%d\n", i, array[i]);
}

