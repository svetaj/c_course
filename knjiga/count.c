#include <stdio.h>

struct key {
        char *keyword;
        int keycount;
} keytab[] = {
        "break", 0,
        "case", 0,
        "char", 0,
        "continue", 0,
        "default", 0,
        /* .... */
        "unsigned", 0,
        "while", 0
};

#define NKEYS (sizeof(keytab) / sizeof(struct key))
#define MAXWORD 20
#define LETTER 'a'
#define DIGIT '0'

main() /* count C keywords */
{
        int n, t;
        char word[MAXWORD];

        while ((t = getword(word, MAXWORD)) != EOF)
                if (t == LETTER)
                        if ((n = binary(word, keytab, NKEYS)) >= 0)
                                keytab[n].keycount++;
        for (n = 0; n < NKEYS; n++)
                if (keytab[n].keycount > 0)
                        printf("%4d %s\n",
                           keytab[n].keycount, keytab[n].keyword);
}

binary(char *word, struct key tab[], int n)     /* find word in tab[] */
{
        int low, high, mid, cond;

        low = 0;
        high = n - 1;
        while (low <= high) {
                mid = (low+high) / 2;
                if ((cond = strcmp(word, tab[mid].keyword)) < 0)
                        high = mid -1;
                else if (cond > 0)
                        low = mid + 1;
                else
                        return(mid);
        }
        return(-1);
}

getword(char *w, int lim)       /* get next word from input */
{
        int c, t;

        if (type(c = *w++ = getch()) != LETTER) {
                *w = '\0';
                return(c);
        }
        while (--lim > 0) {
                t = type(c = *w++ = getch());
                if (t != LETTER && t != DIGIT) {
                        ungetch(c);
                        break;
                }
        }
        *(w-1) = '\0';
        return(LETTER);
}

type(int c)     /* return type of ASCII character */
{
        if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
                return(LETTER);
        else if (c >= '0' && c <= '9')
                return(DIGIT);
        else
                return(c);
}




