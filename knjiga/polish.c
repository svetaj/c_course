#include <stdio.h>
#include "polish.h"

main()                  /* reverse Polish desk calculator */
{
        int type;
        char s[MAXOP];
        double op2, atof(), pop(), push();

        while ((type = getop(s, MAXOP)) !=EOF)
                switch (type)
                {
                case NUMBER:
                        /* printf("##### BROJ ##### \n"); */
                        push (atof(s));
                        break;
                case '+':
                        /* printf("##### + OPERATOR ##### \n"); */
                        push (pop() + pop());
                        break;
                case '*':
                        /* printf("##### * OPERATOR ##### \n"); */
                        push (pop() * pop());
                        break;
                case '-':
                        /* printf("##### - OPERATOR ##### \n"); */
                        op2 = pop();
                        push(pop() - op2);
                        break;
                case '/':
                        /* printf("##### / OPERATOR ##### \n"); */
                        op2 = pop();
                        if (op2 != 0.0)
                                push (pop() / op2);
                        else
                                printf("Deljenje nulom nije dozvoljeno\n");
                        break;
                case '=':
                        /* printf("##### = OPERATOR ##### \n"); */
                        printf("\t%f\n", push(pop()));
                        break;
                case 'c':
                        clear();
                        break;
                case TOOBIG:
                        printf("%.20s ... je suvise dugacak\n", s);
                        break;
                default:
                        printf("Komanda %c ne postoji\n", type);
                        break;
                }
}
