/* testira upotrebu primitivnih verzija funkcija alloc() i free() */

#include <stdio.h>            /* zbog printf() */
#include <string.h>           /* zbog strcat() */

char *alloc(int);             /* prototip funkcije alloc() */

main()
{
        char *abc;
        int i;

        abc = alloc(100);
        for (i=0; i<10; i++) {
                strcat (abc, "Dobar dan");
                printf("abc=%s\n", abc);
        }
        free(abc);
}

