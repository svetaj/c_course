#include <stdio.h>

/* Verzija II, koristi osobinu da alazni argument funkcije */
/* zadat preko vrednosti ostaje ne promenjen */

main()	/* testira funkciju stepen */
{
	int i;

	for (i = 0; i < 10; ++i)
		printf ("%d %d %d\n", i, power(2,i), power(-3,i));
}

power(x, n)	/* podize x na n-ti stepen; n > 0 */
int x, n;
{
        int p;

        for (p = 1; n > 0; --n)
		p = p * x;
	return(p);
}
