#include <stdio.h>

main()
{
        int i;
        int x[10] = {13, 51, 28, 11, 444, 22, 37, 16, 91, 1};

        printf ("Vektor PRE sortiranja \n");
        for (i=0; i<10; i++)
                printf ("x[%d]=%d ", i, x[i]);
        printf ("\n");

        shell (x, 10);
        printf ("Vektor POSLE sortiranja \n");
        for (i=0; i<10; i++)
                printf ("x[%d]=%d ", i, x[i]);
        printf ("\n");

}

shell(v, n)             /* sort v[0] ... v[n-1] into increasing order */
int v[], n;
{
        int gap, i, j, temp;

        for (gap = n/2; gap> 0; gap /=2)
                for (i = gap; i < n; i++)
                        for (j=i-gap; j>=0 && v[j] > v[j+gap]; j-=gap) {
                                temp = v[j];
                                v[j] = v[j+gap];
                                v[j+gap] = temp;
                        }
}

