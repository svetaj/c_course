/* sort strings v[0] ... v[n-1] */
/* into increasing order        */

void sort(char *v[], int n, int (*comp)(), void (*exch)())
{                              
        int gap, i, j;

        for (gap = n/2; gap > 0; gap /=2)
             for (i = gap; i < n; i++)
                for ( j = i-gap; j >= 0; j -= gap) {
                        if ((*comp)(v[j], v[j+gap]) <= 0)
                                break;
                        (*exch)(&v[j], &v[j+gap]);
                }
}


