#include <stdio.h>

#define LINES  100   /* max lines to be sorted */

int readlines(char *l[], int);
void writelines(char *l[], int);
void sort(char *l[], int, int (*f)(), void (*g)());
int strcmp(char *, char *);
int numcmp(char *, char *);
void swap(char *l[], char *j[]);

void main(int argc, char *argv[])  /* sort input lines */
{
        char *lineptr[LINES];   /* pointers to text lines     */
        int nlines;             /* number of input lines read */
        int numeric = 0;        /* 1 if numeric sort          */

        if (argc>1 && argv[1][0] == '-' && argv[1][1] == 'n')
                numeric = 1;

        if ((nlines = readlines(lineptr, LINES)) >= 0) {
                if (numeric)
                        sort(lineptr, nlines, numcmp, swap);
                else
                        sort(lineptr, nlines, strcmp, swap);
                writelines(lineptr, nlines);
        }
        else
                printf("input too big to sort\n");
}


void swap(char *px[], char *py[])
{
        char *temp;

        temp = *px;
        *px = *py;
        *py = temp;
}

