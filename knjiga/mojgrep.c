#include <stdio.h>
#define MAXLINE 1000

int getline(char s[], int lim);
index(char s[], char t[]);

main(int argc, char *argv[]) /* find pattern from first argument */
{
   char line[MAXLINE];

   if (argc != 2)
        printf("Usage: %s pattern\n", argv[0]);
   else
        while (getline(line, MAXLINE) > 0)
        if (index(line, argv[1]) >= 0)
              printf("%s", line);
}
