#include <stdio.h>

struct tnode {                   /* the basic node        */
        char *word;               /* points to the text    */
        int count;               /* number of occurrences */
        struct tnode *left;      /* left child            */
        struct tnode *right;     /* right child           */
};

struct tnode *talloc();
struct tnode *tree();
char *strsave();
char *alloc();


#define LETTER 'a'
#define DIGIT  '0'

#define MAXWORD 20

main()          /* word frequency count */
{
        struct tnode *root;
        char word[MAXWORD];
        int t;

        root = NULL;
        while ((t = getword(word, MAXWORD)) != EOF)
                if (t == LETTER)
                        root = tree(root, word);
        treeprint(root);
}

struct tnode *tree (struct tnode *p, char *w)   /* install w at or below p */
{
        int cond;

        if (p == NULL) {                    /* a new word has arrived       */
            p = talloc();                   /* make a new node              */
            p->word = strsave(w);
            p->count = 1;
            p->left = p->right = NULL;
        } else if ((cond = strcmp (w, p->word)) == 0)
            p->count++;                     /* repeated word                */
        else if (cond < 0)                  /* lower goes into left subtree */
            p->left = tree(p->left, w);
        else                                /* greather into right subtree  */
            p->right = tree(p->right, w);
        return(p);
}


treeprint(struct tnode *p)
{
        if (p != NULL) {
                treeprint (p->left);
                printf ("%4d %s\n", p->count, p->word);
                treeprint (p->right);
        }
}

struct tnode *talloc()
{
        return ((struct tnode *) alloc (sizeof (struct tnode)));
}

char *strsave(char *s)
{
        char *p;

        if ((p = alloc (strlen(s) + 1)) != NULL)
                strcpy (p, s);
        return(p);
}


getword (char *w, int lim)       /* get next word from input */
{
        int c, t;

        if (type(c = *w++ = getch()) != LETTER) {
                *w = '\0';
                return(c);
        }
        while (--lim > 0) {
                t = type (c = *w++ = getch());
                if (t != LETTER && t != DIGIT) {
                        ungetch(c);
                        break;
                }
        }
        *(w-1) = '\0';
        return (LETTER);
}

type(int c)         /* return type of ASCII character */
{
        if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
                return (LETTER);
        else if (c >= '0' && c <= '9')
                return (DIGIT);
        else
                return (c);
}





