#include <stdio.h>

char *month_name(int);
char *ime_meseca(int);

void main()
{
        int m;

        m = 6;
        print_mesec(month_name, m);
        m = 10;
        print_mesec(ime_meseca, m);
}

void print_mesec(char *(*f)(), int mesec)
{
        printf("Mesec broj %d je %s\n", mesec, (*f)(mesec));
}

char *month_name(int n)         /* engleska verzija */
{
        static char *name[] = {
                "illegal month",
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
        };

        return( (n < 1 || n > 12) ? name[0] : name[n]);
}

char *ime_meseca(int n)         /* srpska verzija */
{
        static char *name[] = {
                "nema gu",
                "Januar",
                "Februar",
                "Mart",
                "April",
                "Maj",
                "Jun",
                "Jul",
                "Avgust",
                "September",
                "Oktobar",
                "Novembar",
                "Decembar"
        };

        return( (n < 1 || n > 12) ? name[0] : name[n]);
}

