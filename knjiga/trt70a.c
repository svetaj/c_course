#include <stdio.h>

int atoi(char s[]);

main() /* proverava upotrebu funkcije atoi() */
{
        char a[40] = "   -123456";
        int i;

        i = atoi(a);
        printf("a=%s   i=%d\n", a, i);
}


int atoi(char s[])     /* convert string s to integer */
{
        double atof();           /* definise izlaz funkcije
                                   (ako ne stavimo prototip) */
        return(atof(s)); 
}

double atof(char s[])  /* convert string s to double */
{
        double val, power;
        int i, sign;

        for (i=0; s[i]==' ' || s[i]=='\n' || s[i]=='\t'; i++)
                ;               /* skip white space */
        sign = 1;
        if (s[i] == '+' || s[i] == '-')         /* sign */
                sign = (s[i++]=='+') ? 1 : -1;
        for (val = 0; s[i] >= '0' && s[i] <= '9'; i++)
                val = 10 * val + s[i] -'0';
        if (s[i] == '.')
                i++;
        for (power = 1; s[i] >= '0' && s[i] <= '9'; i++) {
                val = 10 * val + s[i] - '0';
                power *= 10;
        }
        return (sign * val /power);
}


