#include <stdio.h>

int strlen(char s[]);

main()
{
        char a[] = "Dobar dan i laku noc";

        reverse(a);
        printf("%s\n", a);
}

reverse(char s[])       /* reverse string s in place */
{
        int c, i, j;

        for ( i= 0, j = strlen(s) - 1; i < j; i++, j--) {
                c = s[i];
                s[i] = s[j];
                s[j] = c;
        }
}

int strlen(char s[]) /* return length of s */
{
        int i;

        i = 0;
        while (s[i] != '\0')
                ++i;
        return(i);
}


