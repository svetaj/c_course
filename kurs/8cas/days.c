//8.2a
#include <stdio.h>

void mesec_dan(int, int, int *, int *);
int dan_u_godini(int, int, int);

main()
{
        int god, mes, dan;
        int brdana;

        god = 4;
        mes = 2;
        dan = 29;
        brdana = dan_u_godini(god, mes, dan);
        printf("%d/%d/%d je %d dan u godini\n", dan, mes, god, brdana);

        mesec_dan(god, brdana, &mes, &dan);
        printf("%d/%d/%d je %d dan u godini\n", dan, mes, god, brdana);
}

