//8.1 alloc.c
#include <stdio.h>
#include <string.h>
                        //char x alloc(int);
                        //free(char *);
#define NULL 0
#define ALLOCSIZE 1000

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;

char *alloc(int n)
{
        if (allocp + n <=allocbuf + ALLOCSIZE) {
                allocp += n;
                return (allocp -n);
        }
        else
                return (NULL);
}

void free (char *p)
{
        int x;
        for(x = 0; x < 50; x++)
                printf("%c", allocbuf[x]);
        if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
                allocp = p;
}

main()
{
        char *x;
        char *y;
        x = alloc(10);
        y = alloc(30);
        strcpy(x, "abcd");
        printf("x = %s\n", x);
        strcpy(y, "1234");
        printf("y = %s\n", y);
        free(y);
        free(x);
                

}

