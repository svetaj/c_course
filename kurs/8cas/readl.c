#include <stdio.h>

#define MAXLEN 1000

char *alloc(int);
void free (char *);
getline(char s[], int lim);
void strcpy(char *, char *);

int readlines(char *lineptr[], int maxlines)  /* read input lines for sorting */
{
        int len, nlines;
        char *p, *alloc(), line[MAXLEN];

        nlines = 0;
        while ((len = getline(line, MAXLEN)) > 0)
                if (nlines >= maxlines)
                        return(-1);
                else if ((p = alloc(len)) == NULL)
                        return(-1);
                else {
                        line[len-1] = '\0';     /* zap newline */
                        strcpy(p, line);
                        lineptr[nlines++] = p;
                }
        return(nlines);
}


