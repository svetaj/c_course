//8.2b
static int tabela_dana[2][13] = {
        { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
        { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
};

int dan_u_godini(int godina, int mesec, int dan)
{
        int i, prestupna;

        prestupna = godina%4 == 0 && godina%100 != 0 || godina%400 == 0;
        for (i = 1; i< mesec; i++)
                dan += tabela_dana[prestupna][i];
        return(dan);
}

void mesec_dan(int godina, int danugod, int *pmesec, int *pdan)
{
        int i, prestupna;

        prestupna = godina%4 == 0 && godina%100 != 0 || godina%400 == 0;
        for (i = 1; danugod > tabela_dana[prestupna][i]; i++) 
                danugod -= tabela_dana[prestupna][i];
        *pmesec = i;
        *pdan = danugod;
}

