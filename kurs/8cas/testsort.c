//8.3a testsort.c
#include <stdio.h>

#define LINES  100   /* max lines to be sorted */

int readlines(char *l[], int);
void writelines(char *l[], int);
void sort(char *l[], int);
 

void main()  /* sort input lines */
{
        char *lineptr[LINES];   /* pointers to text lines */
        int nlines;             /* number of input lines read */

        if ((nlines = readlines(lineptr, LINES)) >= 0) {
                sort(lineptr, nlines);
                writelines(lineptr, nlines);
        }
        else
                printf("input too big to sort\n");
}


