//8.5
#include <stdio.h>

char *month_name(int);

void main()
{
        int m;

       // m = 1;
       for (m = 1; m <= 12; m++)
       //    ;
       //         m++;           
       // m = 6;
        printf("Mesec broj %d je %s\n", m, month_name(m));
}

char *month_name(int n)         /* return name of n-th month */
{
        static char *name[] = {
                "illegal month",
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
        };

        return( (n < 1 || n > 12) ? name[0] : name[n]);
}

