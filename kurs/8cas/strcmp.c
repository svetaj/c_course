int strcmp(char *s, char *t)   /* return <0 if s<t, 0 if s==t, >0 if s>t */
{                              /* pointer version */
        int i;

        for ( ; *s == *t; s++, t++)
                if (*s == '\0')
                        return(0);
        return(*s - *t);
}

