//11.4 file.c
//kopira FAJL1 u FAJL2

#include <stdio.h>

main()
{
        int c;
        FILE *fp1, *fp2;
        fp1 = fopen("FAJL1", "r");
 // ili fp1 = fopen("C:\\PATH\\FAJL1", "r") za DOS
 // "r" => read only
        fp2 = fopen("FAJL2", "w");
 // "w" => write
        while ((c = getc(fp1)) != EOF)
                putc(c, fp2);
        fclose(fp1);
        fclose(fp2); //zatvara fajl
}
