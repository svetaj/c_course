//11.x

#include <fcntl.h>
#include <sys/stat.h>
#define BUFSIZE 512

main()
{
        char buf[BUFSIZE];
        int n;
        int fp1, fp2;
        fp1 = open("FAJL1", O_RDONLY);
        fp2 = open("FAJL2", O_CREAT);
	// ili O_CREAR zamenimo sa O_RDWR za citanje i pisanje
	// ako stavim O_RDWR|O_BINARI vaze oba
        while((n = read(fp1, buf, BUFSIZE)) > 0)
                write(fp2, buf, n);
        close(fp1);
        close(fp2);
}
