//11.2
#include <stdio.h>

main()
{
        printf(":%10s:\n", "hello, world");
        printf(":%-10s:\n", "hello, world");
        printf(":%20s:\n", "hello, world");
        printf(":%-20s:\n", "hello, world");
        printf(":%20.10s:\n", "hello, world");
        printf(":%-20.10s:\n", "hello, world");
        printf(":%.10s:\n", "hello, world");
}
