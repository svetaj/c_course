#include <string.h>              
#include <stdio.h>
#include <mem.h>
#include <alloc.h>


int main(void)
{
   char *buffer;

   buffer = (char *) malloc (51);
   memset (buffer, '*' , 50);
   buffer [50] = 0;

   printf("\n\n\nBuffer after memset 1:  %s\n", buffer);
   memset(buffer, 'A', 3);
   printf("Buffer after memset 2:  %s\n", buffer);
   memset(buffer + 3, 70, 3);
   printf("Buffer after memset 3:  %s\n", buffer);

   return 0;
}