/* 7/3 ili 2/x pronalazi broj karaktera u nizu */
#include <stdio.h>

main()
{
        char tekst[] = "Ovo je tekst";
        int i;
        i = strlen(tekst);
        printf("duzina = %d\n", i);
}

int strlen(char s[])
{
        int i;
        i = 0;
        while (s[i] != '\0')
                ++i;
        return(i);
}
