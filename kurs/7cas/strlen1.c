/* 7/3a pronalazi broj karaktera u nizu */
#include <stdio.h>
// int strlen(char *s);

main()
{
        char tekst[] = "Ovo je tekst";
        int i;
        i = strlen(tekst);
        printf("duzina = %d\n", i);
}

int strlen(char *s)
{
        int n;
        for (n = 0; *s != '\0'; s++)
                n++;
        return(n);
}
