#include <stdio.h>

main()
{
        int i, j;
        i = 35; j = 1;
        printf("i = %d, j = %d\n", i, j);
        obrni(&i, &j);
        printf("i = %d, j = %d\n", i, j);        
}

obrni (int *x, int *y)
{
        int temp;
        temp = *x;
        *x = *y;
        *y = temp;
}
