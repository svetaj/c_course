/* 3/1 if.c */
#include <stdio.h>
main()
{
        int i, j;
        i = 0;
        j = 10;
        if (i == 0)     /* ili if (!i) */
                printf("i jednako 0\n");
        if (j)          /* ili if(j > 0), ili if(!(j <= 0)) */
                printf("j vece od 0\n");

}
