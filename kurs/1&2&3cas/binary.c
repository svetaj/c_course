/* 3/6 binarno pretrazivanje */
#include <stdio.h>

binary ( int x, int v[], int n)
{
        int low, high, mid;
        low=0;
        high = n -1;
        while ( low <= high)
        {
                mid = (low + high)/2;
                if (x < v[mid])
                        high = mid -1;
                else if (x > v[mid])
                        low = mid + 1;
                else    /* found match */
                        return (mid);
        }
        return(-1);
}

main()
{
        int i;
        int x[10] = {1, 11, 13, 16, 22, 28, 37, 51, 91, 444};

        /* 1.       low            mid                  high */
        /* 2.                          low      mid     high */
        /* 3.                          low |6|  high         */

        i = 37;
        printf("Vrednost %d pronadji na mestu %d\n", i, binary(i, x, 10));
}
