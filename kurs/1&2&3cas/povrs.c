#include <stdio.h>
#include <math.h>
#define PI 3.14159
#define SQUARE(x) ((x) * (x))
double area;
double area_of_circle (double);

main(int argc,char **argv)
{
        double radius;
        if (argc<2) exit (0);
        radius=atof(argv[1]);
        area=area_of_circle(radius);
        printf("Povrsina=%f ,R=%f\n",radius,area);
        
}

double area_of_circle(double r)
{
        return(4.0*PI*SQUARE(r));
}
