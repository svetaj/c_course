/* 3/6 + 4/4 binarno pretrazivanje */
#include <stdio.h>

shell(int v[], int n)
{
        int gap, i, j, temp;
        for (gap = n/2; gap > 0; gap /= 2)
                for (i = gap; i < n; i++)
                        for(j=i-gap; j >= 0 && v[j] > v[j+gap]; j -= gap)
                        {
                                temp = v[j];
                                v[j] = v[j + gap];
                                v[j + gap] = temp;
                        }
                        
}

binary ( int x, int v[], int n)
{
        int low, high, mid;
        low=0;
        high = n -1;
        while ( low <= high)
        {
                mid = (low + high)/2;
                if (x < v[mid])
                        high = mid -1;
                else if (x > v[mid])
                        low = mid + 1;
                else    /* found match */
                        return (mid);
        }
        return(-1);
}

main()
{
        int i;
        int x[10] = {13, 51, 28, 11, 444, 22, 37, 16, 91, 1};
        printf("Vektor pre sortiranja\n");
        for(i = 0; i < 10; i++)
        printf("x[%d] = %d\n", i, x[i]);
        printf("\n");
        shell(x,10);
        printf("x[%d] = %d\n", i, x[i]);
        printf("\n");

        
        i = 37;
        printf("Vrednost %d pronadji na mestu %d\n", i, binary(i, x, 10));
}
