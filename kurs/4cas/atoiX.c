/* atoi.c 4/1 */
#include <stdio.h>

atoi (char s[])
{
        int i, n, sign;
        for(i = 0; s[i] ==' ' || s[i]=='\n' || s[i]=='\t'; i++)
                ;
        sign=1;
        if (s[i] == '+' || s[i] == '-')   /* znak */
                sign = (s[i++] == '+') ? 1 : - 1;
        for ( n = 0; s[i] >= '0' && s[i] <= '9'; i++)
                n = 10 * n + s[i] - '0';
        return(sign * n);
}

main()
{
        int x;
        char a[]="   -135";
        x=atoi(a);
        printf("%d\n",x);
        return 0;
}
