/* 4.3 continue  */
#include <stdio.h>
main()
{
        int i;
        for( i = 10; i < 100; i += 10)
        {
                if (i == 70)
                        break;
                if (i == 30 || i == 50)
                        continue;                
                printf("%d\n", i);
         }
}
