//10.1 kompl.c
#include <stdio.h>

struct kompl
{
        double x;
        double y;
};

typedef struct kompl KMP;
struct kompl saberi( KMP a, KMP b)
{
        KMP c;
        c.x = a.x + b.x;
        c.y = a.y + b.y;
        return c;
}

main()
{
        KMP p = {-3, 8};
        KMP q = {2, -4};
        KMP r;
        r = saberi(p, q);
        printf("r = (%f, %f)\n", r.x, r.y);

}
