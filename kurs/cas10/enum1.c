//10.2a enum.c sa typedef
#include <stdio.h>
enum Dani
{
        pon = 1,   //krece od 1, a ne od 0
        uto,   
        sre,  
        cet,     
        pet,      
        sub,      
        ned   
};

typedef enum dani DD;

main()
{
        DD x;
        x = cet;
        printf("x = %d\n", x);
        if (x == cet)
                printf("cetvrtak\n");
}
