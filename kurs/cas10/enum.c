//10.2 enum.c
#include <stdio.h>
enum Dani
{
        pon = 1,   //krece od 1, a ne od 0
        uto,   
        sre,  
        cet,     
        pet,      
        sub,      
        ned   
};

main()
{
        enum Dani x;
        x = cet;
        printf("x = %d\n", x);
        if (x == cet)
                printf("cetvrtak\n");
}
