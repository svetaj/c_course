//5.7
#include <stdio.h>

abc()
{
        static int i=0;   //i=0 se izvrsava samo 1. put zbog funkcije
        ++i;              // ali to ne moramo staviti jer se to
                          //     podrazumeva
        printf("%d\n", i);
}

main()
{
        abc();
        abc();
        abc();
}
