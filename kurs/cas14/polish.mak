all: polish.exe

polish.obj: polish.c polish.h
        bcc -c polish.c

iolib.obj: iolib.c
        bcc -c iolib.c

pushpop.obj: pushpop.c
        bcc -c pushpop.c

getop.obj: getop.c
        bcc -c getop.c

atof.obj: atof.c
        bcc -c atof.c

polish.exe: polish.obj iolib.obj pushpop.obj getop.obj atof.obj
        bcc -epolish polish.obj iolib.obj pushpop.obj getop.obj atof.obj






