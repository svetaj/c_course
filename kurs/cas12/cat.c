//12.1 cat.c

#include <stdio.h>
main (int argc,char *argv[])
{
        FILE *fp;
        if (argc==1)  /* nema argumenata */
           filecopy(stdin);
        else
           while (--argc>0)
           if ((fp=fopen(*++argv,"r"))==NULL){
            printf("cat:cann't open %s\n",*argv);
            break;  //prekida stampanje drugog fajla ako postoji
          }else {
             filecopy(fp);
             fclose(fp);
             }
}
filecopy(FILE *fp)
{
        int c;
        while ((c=getc(fp)) !=EOF)
             putc(c,stdout);
}
