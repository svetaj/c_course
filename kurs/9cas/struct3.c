//9.4 struct3.c
#include <stdio.h>
prikaz (struct datum *d);
struct datum                     //datum je struktura koja
{                                //sadrzi elemente dan, mesec, 
        int dan;                 //godina
        int mesec;
        int godina;
};

main()
{
        struct datum rodjen;     //rodjen je ime promenljive
        struct datum x = {05, 05, 1990};
        rodjen.dan = 11;         //dan, mesec, godina
        rodjen.mesec = 11;       //su elementi strukture
        rodjen.godina = 1999;
        prikaz(&rodjen);
        prikaz(&x);
}

prikaz (struct datum *d)
{
        printf("%d/%d/%d\n",
                d->dan,
                d->mesec,
                d->godina);
}
