//9.5 kompl.c
#include <stdio.h>

struct kompl
{
        double x;
        double y;
};

struct kompl saberi( struct kompl a, struct kompl b)
{
        struct kompl c;
        c.x = a.x + b.x;
        c.y = a.y + b.y;
        return c;
}

main()
{
        struct kompl p = {-3, 8};
        struct kompl q = {2, -4};
        struct kompl r;
        r = saberi(p, q);
        printf("r = (%f, %f)\n", r.x, r.y);

}
