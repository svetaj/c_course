//9.1 struct.c
#include <stdio.h>
struct datum                     //datum je struktura koja
{                                //sadrzi elemente dan, mesec, 
        int dan;                 //godina
        int mesec;
        int godina;
};

main()
{
        struct datum rodjen;     //rodjen je ime promenljive
        rodjen.dan = 11;         //dan, mesec, godina
        rodjen.mesec = 11;       //su elementi strukture
        rodjen.godina = 1999;
        printf("%d/%d/%d\n",
                rodjen.dan,
                rodjen.mesec,
                rodjen.godina);
}
